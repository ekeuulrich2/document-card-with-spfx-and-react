import * as React from 'react';
import styles from './DocumentCardExample.module.scss';
import { IDocumentCardExampleProps } from './IDocumentCardExampleProps';
import { escape } from '@microsoft/sp-lodash-subset';
import {
  DocumentCard,
  DocumentCardPreview,
  DocumentCardTitle,
  DocumentCardActivity,
  IDocumentCardPreviewProps
} from 'office-ui-fabric-react/lib/DocumentCard';

export default class DocumentCardExample extends React.Component<IDocumentCardExampleProps, {}> {
  public render(): JSX.Element {

    const previewProps: IDocumentCardPreviewProps = {
      previewImages: [
        {
          previewImageSrc: String(require('./Burger-King.jpg')),
          iconSrc: String(require('./icon-bk.png')),
          width: 318,
          height: 196,
          accentColor: '#ce4b1f'
        }
      ],
    };

    return (
      <DocumentCard onClickHref='https://www.bk.com'>
        <DocumentCardPreview { ...previewProps } />
        <DocumentCardTitle title='Découvrez les nouveautés Burger King® à ne pas manquer' />
        <DocumentCardActivity
          activity= 'Created November 6, 2019'
          people= {
            [
              {name: 'Ulrich', profileImageSrc: require('./king-burger-cartoon-vector.jpg')}
            ]
          }/>
      </DocumentCard>
    );
  }
}
